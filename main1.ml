type nucleotide = A|C|G|T
type brin = nucleotide list

let rec contenu_gc brin =
  let rec aux brin cptr_gc cptr= 
    match brin with
    | [] -> cptr_gc/.cptr
    | t::reste -> if (t = C || t = G) then aux reste (cptr_gc +.1.0) (cptr+.1.0)
        else aux reste cptr_gc (cptr +.1.0);
  in aux brin 0.0 0.0;;

let rec brin_complementaire brin = 
  match brin with
  | [] -> []
  | t::reste -> match t with
    | A -> T::(brin_complementaire reste)
    | T -> A::(brin_complementaire reste)
    | C -> G::(brin_complementaire reste)
    | G -> C::(brin_complementaire reste) ;;
